﻿using System;
using Microsoft.Xna.Framework;

namespace GameDevFriendsEngine
{
    public class SimpleBehavior : Component
    {
        public override void OnCreate()
        {
            Console.WriteLine("I was just created!");
        }

        public override void OnUpdate()
        {
            Console.WriteLine("I was updated!");
        }
    }
}
