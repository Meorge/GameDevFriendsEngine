﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

using System.Collections.Generic;

namespace GameDevFriendsEngine
{
    public class GameObject
    {
        public string Name { get; set; } = "Game Object";

        public Vector3 Position { get; set; } = Vector3.Zero;
        public Vector2 Scale { get; set; } = Vector2.One;
        public float Rotation { get; set; } = 0f;

        // This List will hold all of the Components attached to
        // this GameObject.
        public List<Component> Components = new List<Component>();

        // This method will run the OnUpdate() function for every
        // Component attached to this GameObject.
        public void OnUpdate()
        {
            // This runs the InternalUpdate() function for every component
            // attached to this GameObject.
            // It uses an 'expression lambda', which you can learn more about at
            // https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/operators/lambda-expressions
            Components.ForEach((c) => c.InternalUpdate());
        }
    }


    public class Component
    {
        // This variable tracks whether or not we've called OnCreate() yet.
        private bool onCreateMethodHasRun = false;

        virtual public void OnCreate() { }
        virtual public void OnUpdate() { }

        public void InternalUpdate()
        {
            // Only run the OnCreate() method once, the first time
            // that InternalUpdate() is called. After that, just run
            // OnUpdate() instead.
            if (!onCreateMethodHasRun)
            {
                OnCreate();
                onCreateMethodHasRun = true;
            }

            else OnUpdate();
        }
    }
}
