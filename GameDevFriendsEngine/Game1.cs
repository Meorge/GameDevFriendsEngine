﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using System;
using System.Collections.Generic;

namespace GameDevFriendsEngine
{
    public class Game1 : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        // This is our List of GameObjects we'll iterate over every frame.
        private List<GameObject> gameObjects = new List<GameObject>();

        public Game1()
        {
            // This is added automatically by MonoGame. You don't need to worry
            // about it at the moment.
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            base.Initialize();

            // Here we create our new GameObject and Component,
            GameObject myGameObject = new GameObject();
            SimpleBehavior myComponent = new SimpleBehavior();

            // then attach the Component to the GameObject,
            myGameObject.Components.Add(myComponent);

            // and finally, add the GameObject to our List.
            gameObjects.Add(myGameObject);
        }

        protected override void LoadContent()
        {
            // This is added automatically by MonoGame. You don't need to worry
            // about it at the moment.
            _spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        protected override void Update(GameTime gameTime)
        {
            // Here, we'll run the OnUpdate() method for every GameObject in
            // our List, in a manner much like how we are running the Components'
            // OnUpdate() functions (using expression lambdas).
            gameObjects.ForEach((gameObject) => gameObject.OnUpdate());

            // This is automatically added by MonoGame.
            // Basically, it just says "quit the game if the player presses
            // Escape on their keyboard, or the Back button on their game
            // controller".
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            base.Draw(gameTime);
        }
    }
}
